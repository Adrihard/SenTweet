package FeatureSelection;

import FeatureExtraction.FeatureStats;

public interface SelectionStep {
    public FeatureStats execute(FeatureStats features);
}