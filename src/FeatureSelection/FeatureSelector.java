package FeatureSelection;

import FeatureExtraction.FeatureStats;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adrihard
 */
public class FeatureSelector
{
    private final List<SelectionStep> _selectionSteps;

    public FeatureSelector() {
        this._selectionSteps = new ArrayList<>();
    }
    
    public FeatureSelector(List<SelectionStep> aSelectionSteps) {
        this._selectionSteps = new ArrayList<>(aSelectionSteps);
    }
    
    public FeatureStats selectFeatures(FeatureStats aStats)
    {
        FeatureStats results = aStats;
        
        for (SelectionStep ss : _selectionSteps)
            results = ss.execute(results);
        
        return (results);
    }

    public void addSelectionStep(SelectionStep aSelectionStep) {
        this._selectionSteps.add(aSelectionStep);
    }
    
    public void addSelectionSteps(List<SelectionStep> aSelectionSteps) {
        this._selectionSteps.addAll(aSelectionSteps);
    }
}
