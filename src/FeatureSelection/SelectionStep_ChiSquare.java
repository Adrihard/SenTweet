package FeatureSelection;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import DocumentClassification.DocumentLabel;
import FeatureExtraction.FeatureStats;
import java.util.HashSet;


public class SelectionStep_ChiSquare implements SelectionStep
{
    private double _criticalValue;

    public SelectionStep_ChiSquare(double aCriticalValue) {
        this._criticalValue = aCriticalValue;
    }
    
    public double getCriticalValue() {
        return this._criticalValue;
    }

    public void setCriticalValue(double aCriticalValue) {
        this._criticalValue = aCriticalValue;
    }

    @Override
    public FeatureStats execute(FeatureStats aStats)
    {
        FeatureStats result = new FeatureStats(aStats);
        
        Map<String, Double>                         selectedFeatures;
        Set<String>                                 featuresToRemove;
        Map<String, Map<DocumentLabel, Long>>       fcjc;
        Map<DocumentLabel, Long>                    categoryCount;
        long                                        N;

        selectedFeatures    = new HashMap();
        fcjc                = result.getFeatureCategoryJointCount();
        featuresToRemove    = new HashSet<>(fcjc.keySet());
        categoryCount       = result.getCategoryCount();
        N                   = result.getN();
        
        //for each feature
        for (Map.Entry<String, Map<DocumentLabel, Long>> featureEntry : fcjc.entrySet())
        {
            Map<DocumentLabel, Long> jointCounts = featureEntry.getValue();

            //N1. is the number of occurrences of the feature
            long N1dot = 0;
            
            for (Long count : jointCounts.values())
                N1dot += count;

            //N0. is the number of occurrences other than this feature
            long N0dot = N - N1dot;
            
            
            //for each category
            for (Map.Entry<DocumentLabel, Long> categoryEntry : featureEntry.getValue().entrySet())
            {
                DocumentLabel category = categoryEntry.getKey();
                
                //N11 is the number of occurrences of the feature in the specific category
                long N11 = categoryEntry.getValue();
                
                //N01 is the number of occurrences of other features in the specific category
                long N01 = categoryCount.get(category) - N11;
                
                //N10 is the number of occurrences of the feature in the other categories
                long N10 = N1dot - N11;
                
                //N00 is the number of occurrences of other features in the other categories
                long N00 = N0dot - N01;
                
                //calculate the chisquare score based on the above statistics
                Double score = 
                    N * Math.pow(N11 * N00 - N10 * N01, 2) /
                    ((N11 + N01) * (N11 + N10) * (N10 + N00) * (N01 + N00));

                //if the score is larger than the critical value then add it to the selected features
                if (score > this.getCriticalValue())
                {
                    String feature          = featureEntry.getKey();
                    Double previousScore    = selectedFeatures.get(feature);
                    
                    if (previousScore == null || previousScore < score)
                        selectedFeatures.put(feature, score);
                }    
            }
        }
        
        featuresToRemove.removeAll(selectedFeatures.keySet());
        
        //cleaning the stats from all the features which have not been selected
        for (String feature : featuresToRemove)
            fcjc.remove(feature);
        
        return (result);
    }
}