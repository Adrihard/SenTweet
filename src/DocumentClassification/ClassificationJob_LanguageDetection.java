package DocumentClassification;

import FeatureExtraction.BinarizedMultinomialFeatureExtractor;
import FeatureSelection.FeatureSelector;
import FeatureSelection.SelectionStep;
import FeatureSelection.SelectionStep_ChiSquare;
import TextImportation.TextImporter_LanguageDetection;
import TextImportation.RawText;
import TextImportation.TextImporter;
import TextProcessing.ProcessingStep;
import TextProcessing.ProcessingStep_LowercaseEverything;
import TextProcessing.ProcessingStep_Trim;
import TextProcessing.TextProcessor;
import TextTokenization.WordNGramTextTokenizer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Stream;

/**
 *
 * @author Adrihard
 */
public class ClassificationJob_LanguageDetection extends ClassificationJob 
{
    @Override
    public void run()
    {
        //Defining Chi square critical value
        //final double CHI_SQUARE_CRITICAL_VALUE = 10.83;//require statistical significance at the 0.001 level. 
        final double CHI_SQUARE_CRITICAL_VALUE = 6.63;//require statistical significance at the 0.01 level.
        
        final double ALPHA = 1;

        //Defining training files paths
        Map<Path, DocumentLabel> lb = new HashMap<>();
        
        lb.put(Paths.get("datasets/training.language.en.txt"), DocumentLabel.English);
        lb.put(Paths.get("datasets/training.language.fr.txt"), DocumentLabel.French);
        lb.put(Paths.get("datasets/training.language.de.txt"), DocumentLabel.German);
        
        //Initializing categories
        DocumentLabel possibleCategories[] = 
        { 
            DocumentLabel.English, 
            DocumentLabel.German, 
            DocumentLabel.French
        };
        
        //Defining text pre-processing steps
        ProcessingStep processingSteps[] = 
        {
            new ProcessingStep_Trim(),
            new ProcessingStep_LowercaseEverything()
        };
        
        //Defining feature selection steps
        SelectionStep selectionSteps[] = {
            new SelectionStep_ChiSquare(CHI_SQUARE_CRITICAL_VALUE)
        };
        
        //Constructing the classifier
        NaiveBayesClassifier nbc = new NaiveBayesClassifier
        (
            new TextProcessor(Arrays.asList(processingSteps)),
            new WordNGramTextTokenizer(1, 1),
            new BinarizedMultinomialFeatureExtractor(),
            new FeatureSelector(Arrays.asList(selectionSteps)),
            ALPHA,
            new HashSet<>(Arrays.asList(possibleCategories))
        );
        
        //Training the classifier
        TextImporter ti = new TextImporter_LanguageDetection();
        Stream<RawText> trainingStream = lb.entrySet().stream()
                .map(entry -> ti.importTexts(entry.getKey(), entry.getValue()))
                .reduce(Stream::concat)
                .orElse(Stream.empty());
        nbc.train(trainingStream);

        //Testing the classifier
        String sentences[] =
        {
            "I am English",
            "Je suis Français",
            "Ich bin Deutsch",
            "Il eu fallu que je le susse",
            "Guerre, sang, douleur, que nos ennemis périssent"
        };

        Stream<RawText> testingStream = Arrays.stream(sentences)
                .map(str -> new RawText(str, DocumentLabel.Unknown));

        nbc.predict(testingStream).peek(prediction -> 
        {
            System.out.format
            (
                "\"%s\" was classified as \"%s\".%n", 
                prediction.getKey().getText(), 
                prediction.getValue()
            );
        }).count();
    }
}
