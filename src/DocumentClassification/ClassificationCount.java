package DocumentClassification;

/**
 *
 * @author Adrihard
 */
public class ClassificationCount
{
    private long _correctlyClassified;
    private long _classified;
    private long _belonging;
    
    public ClassificationCount()
    {
        this._correctlyClassified   =
        this._classified            =
        this._belonging             = 0;
    }
    
    public ClassificationCount
    (
        long aCorrectlyClassified,
        long aClassified,
        long aBelonging
    )
    {
        this._correctlyClassified   = aCorrectlyClassified;
        this._classified            = aClassified;
        this._belonging             = aBelonging;
    }
    
    public void merge(ClassificationCount count)
    {
        this.setCorrectlyClassified(this.getCorrectlyClassified() + count.getCorrectlyClassified());
        this.setClassified(this.getClassified() + count.getClassified());
        this.setBelonging(this.getBelonging() + count.getBelonging());
    }
    
    public long getCorrectlyClassified() {
        return (this._correctlyClassified);
    }
    
    public long getClassified() {
        return (this._classified);
    }
    
    public long getBelonging() {
        return (this._belonging);
    }
    
    public void setCorrectlyClassified(long aCorrectlyClassified) {
        this._correctlyClassified = aCorrectlyClassified;
    }
    
    public void setClassified(long aClassified) {
        this._classified = aClassified;
    }
    
    public void setBelonging(long aBelonging) {
        this._belonging = aBelonging;
    }
    
    public void incrementCorrectlyClassified() {
        this._correctlyClassified++;
    }
    
    public void incrementClassified() {
        this._classified++;
    }
    
    public void incrementBelonging() {
        this._belonging++;
    }
            
}
