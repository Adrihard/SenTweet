package DocumentClassification;

import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author Adrihard
 */
public class ClassificationReport extends HashMap<DocumentLabel, ClassificationCount>
{
    public double getRecall(DocumentLabel aCategory) 
    {
        ClassificationCount cc = this.get(aCategory);
        if (cc.getBelonging() == 0) {
            return 0;
        }
        return ((double) cc.getCorrectlyClassified() / cc.getBelonging());
    }

    public double getRecall()
    {
        Set<DocumentLabel> labels = this.keySet();
        
        double sum = labels.stream()
            .map((key) -> this.getRecall(key))
            .reduce(Double::sum)
            .orElse(0.0);
        
        return (sum / labels.size());
    }
    
    public double getPrecision(DocumentLabel aCategory)
    {
        ClassificationCount cc = this.get(aCategory);
        if (cc.getClassified() == 0) {
            return 0;
        }
        return ((double) cc.getCorrectlyClassified() / cc.getClassified());
    }
    
    public double getPrecision()
    {
        Set<DocumentLabel> labels = this.keySet();
        
        double sum = labels.stream()
            .map((key) -> this.getPrecision(key))
            .reduce(Double::sum)
            .orElse(0.0);
        
        return (sum / labels.size());
    }
    
    public double getFMeasure()
    {
        double precision    = this.getPrecision();
        double recall       = this.getRecall();
        
        return 
        (
            2 * (precision * recall) 
            / (precision + recall)
        );
    }
    
    @Override
    public String toString() {
        return (super.toString());
    };
}
