package DocumentClassification;

public enum DocumentLabel {
    Positive, Negative, Neutral, English, French, German, Unknown;
}