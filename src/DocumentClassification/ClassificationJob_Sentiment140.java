package DocumentClassification;

import FeatureExtraction.BinarizedMultinomialFeatureExtractor;
import FeatureSelection.FeatureSelector;
import FeatureSelection.SelectionStep;
import FeatureSelection.SelectionStep_ChiSquare;
import TextImportation.RawText;
import TextImportation.TweetImporter;
import TextImportation.TweetImporter_Sentiment140;
import TextProcessing.ProcessingStep;
import TextProcessing.ProcessingStep_ExtractWordsOnly;
import TextProcessing.ProcessingStep_LowercaseEverything;
import TextProcessing.TextProcessor;
import TextTokenization.WordNGramTextTokenizer;
import java.math.RoundingMode;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Stream;

/**
 *
 * @author Adrihard
 */
public class ClassificationJob_Sentiment140 extends ClassificationJob {

    @Override
    public void run()
    {
        //Defining Chi square critical value
        //final double CHI_SQUARE_CRITICAL_VALUE = 10.83;//require statistical significance at the 0.001 level. 
        final double CHI_SQUARE_CRITICAL_VALUE = 6.63;//require statistical significance at the 0.01 level.
        final double ALPHA = 1;
        
        
        //Defining training files paths
        String training[] = {
            "datasets/training.1200000.processed.noemoticon.csv"
        };

        //Defining testing files paths
        String testing[] = {
            "datasets/testing.400000.processed.noemoticon.csv"
        };
        
        //Initializing categories
        DocumentLabel possibleCategories[] = 
        { 
            DocumentLabel.Positive, 
            DocumentLabel.Negative, 
            DocumentLabel.Neutral
        };
        
        //Defining text pre-processing steps
        ProcessingStep processingSteps[] = 
        {
            new ProcessingStep_ExtractWordsOnly(),
            new ProcessingStep_LowercaseEverything(),
        };
        
        //Defining feature selection steps
        SelectionStep selectionSteps[] = {
            new SelectionStep_ChiSquare(CHI_SQUARE_CRITICAL_VALUE)
        };
        
        //Constructing the classifier
        NaiveBayesClassifier nbc = new NaiveBayesClassifier
        (
            new TextProcessor(Arrays.asList(processingSteps)),
            new WordNGramTextTokenizer(1, 1),
            new BinarizedMultinomialFeatureExtractor(),
            new FeatureSelector(Arrays.asList(selectionSteps)),
            ALPHA,
            new HashSet<>(Arrays.asList(possibleCategories))
        );

        //Training the classifier
        TweetImporter sti = new TweetImporter_Sentiment140();
        Stream<RawText> trainingStream = Arrays.stream(training)
                .map(name -> Paths.get(name))
                .map(path -> sti.importTexts(path))
                .reduce(Stream::concat)
                .orElse(Stream.empty());
        nbc.train(trainingStream);

        //Testing the classifier
        Stream<RawText> testingStream = Arrays.stream(testing)
                .map(name -> Paths.get(name))
                .map(path -> sti.importTexts(path))
                .reduce(Stream::concat)
                .orElse(Stream.empty());
        
//        nbc.predict(testingStream).peek(prediction -> 
//        {
//            System.out.format
//            (
//                "\"%s\" was classified as \"%s\".%n", 
//                prediction.getKey().getText(),
//                prediction.getValue()
//            );
//        }).count();

        ClassificationReport report = nbc.test(testingStream);
        
        DecimalFormat df = new DecimalFormat("0.###");
        df.setRoundingMode(RoundingMode.CEILING);
        
        System.out.println("Average recall: " + df.format(report.getRecall() * 100) + "%");
        System.out.println("Average precision: " + df.format(report.getPrecision() * 100) + "%");
        System.out.println("F-Measure: " + df.format(report.getFMeasure() * 100) + "%");
    }
    
}
