package DocumentClassification;

import FeatureExtraction.FeatureExtractor;
import FeatureExtraction.FeatureStats;
import FeatureSelection.FeatureSelector;
import TextImportation.RawText;
import TextProcessing.ProcessedText;
import TextProcessing.TextProcessor;
import TextTokenization.Document;
import TextTokenization.TextTokenizer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import javafx.util.Pair;

public class NaiveBayesClassifier implements DocumentClassifier
{
    private final   TextProcessor                           _textProcessor;
    private final   TextTokenizer                           _textTokenizer;
    private final   FeatureExtractor                        _featureExtractor;
    private final   FeatureSelector                         _featureSelector;
    
    private         FeatureStats                            _featureStats;
    
    private final   Set<DocumentLabel>                      _possibleCategories;
    
    private         double                                  _alpha;
    
    private         Map<DocumentLabel, Double>              _logPriors;
    private         Map<String, Map<DocumentLabel, Double>> _logConditionals;
    
    public NaiveBayesClassifier
    (
        TextProcessor           aTextProcessor,
        TextTokenizer           aTextTokenizer,
        FeatureExtractor        aFeatureExtractor,
        FeatureSelector         aFeatureSelector,
        double                  aAlpha,
        Set<DocumentLabel>      aPossibleCategories
    ) 
    {
        this._textProcessor         = aTextProcessor;
        this._textTokenizer         = aTextTokenizer;
        this._featureExtractor      = aFeatureExtractor;
        this._featureSelector       = aFeatureSelector;
        this._alpha                 = aAlpha;
        this._possibleCategories    = aPossibleCategories;
        this._featureStats          = null;
    }
    
    @Override
    public void train(Stream<RawText> aRawTexts) {
        this.trainClassifier(this.processDataset(aRawTexts));
    }

    private FeatureStats processDataset(Stream<RawText> aRawTexts)
    {   
        FeatureStats        stats;

        stats = aRawTexts
                .map(text -> this._textProcessor.processText(text))
                .map(ptext -> this._textTokenizer.tokenizeText(ptext))
                .map(ttext -> this._featureExtractor.extractFeatures(ttext))
                .reduce((stat1, stat2) -> stat1.merge(stat2))
                .orElse(FeatureStats.empty());

        stats       = this._featureSelector.selectFeatures(stats);
        
        return (stats);
    }
    
    private void normalizeFeatureStats(FeatureStats aStats)
    {
        Map<String, Map<DocumentLabel, Long>>   fcjc;
        Map<DocumentLabel, Long>                categoryCount;
        
        fcjc            = aStats.getFeatureCategoryJointCount();
        categoryCount   = aStats.getCategoryCount();
 
        for (DocumentLabel category : this._possibleCategories)
        {
            for (String feature : fcjc.keySet())
            {  
                if (fcjc.get(feature).get(category) == null)
                    fcjc.get(feature).put(category, new Long(0));
            }
            
            boolean emptyCategory = (categoryCount.get(category) == null);
            
            assert(!emptyCategory);
            
            if (emptyCategory)
                categoryCount.put(category, new Long(0));
        }
    }
    
    private void trainClassifier(FeatureStats aStats)
    {
        if (this._featureStats == null)
            this._featureStats = aStats;
        else
            this._featureStats.merge(aStats);
        
        FeatureStats stats = this._featureStats;
        
        this.normalizeFeatureStats(stats);
        
        Map<DocumentLabel, Double>                  logPriors;
        Map<String, Map<DocumentLabel, Double>>     logConditionals;
        
        Map<String, Map<DocumentLabel, Long>>       fcjc;
        Map<DocumentLabel, Long>                    categoryCount;

        logPriors       = new HashMap<>();
        logConditionals = new HashMap<>();        
        
        fcjc            = stats.getFeatureCategoryJointCount();
        categoryCount   = stats.getCategoryCount(); 
        
        Set<String> V = fcjc.keySet();

        //estimating log prior probabilities
        Long        N = stats.getN();
        
        for (DocumentLabel category : this._possibleCategories)
        {    
            Double Nc = categoryCount.get(category).doubleValue();
            
            logPriors.put(category, Math.log(Nc/N));
        }
        
        //We are performing laplace smoothing (also known as add-1). This requires to estimate the total feature occurrences in each category
        Map<DocumentLabel, Long> featureOccurrencesInCategory = new HashMap<>();
        
        for (DocumentLabel category : logPriors.keySet())
        {
            Long featureOccSum = new Long(0);
            
            for(Map<DocumentLabel, Long> categoryListOccurrences : fcjc.values())
                featureOccSum += categoryListOccurrences.get(category);
            
            featureOccurrencesInCategory.put(category, featureOccSum);
        }
        
        //estimate log likelihoods
        for (DocumentLabel category : logPriors.keySet())
        {
            for(Map.Entry<String, Map<DocumentLabel, Long>> entry : fcjc.entrySet())
            {    
                String feature = entry.getKey();
                Map<DocumentLabel, Long> featureCategoryCounts = entry.getValue();
                
                Long count = featureCategoryCounts.get(category);
                
                double logLikelihood = Math.log
                (
                    (count.doubleValue() + this._alpha) /
                    (featureOccurrencesInCategory.get(category) + V.size() * this._alpha)
                );
                
                if(logConditionals.get(feature) == null)
                    logConditionals.put(feature, new HashMap<>());
                
                logConditionals.get(feature).put(category, logLikelihood);
            }
        }
        
        this._logPriors         = logPriors;
        this._logConditionals   = logConditionals;
    }
    
    
    
    @Override
    public Stream<Pair<RawText, DocumentLabel>> predict(Stream<RawText> aRawTexts)
    {
        if (this._featureStats == null)
        {
            throw (new IllegalArgumentException(
                "The classifier must be trained at least once before prediction."
            ));
        }

        Stream<Pair<RawText, DocumentLabel>>     predictions;

        predictions = aRawTexts
                .map(text -> {
                    ProcessedText ptext = this._textProcessor.processText(text);
                    Document ttext = this._textTokenizer.tokenizeText(ptext);
                    DocumentLabel prediction = this.predictOnce(ttext);
                    return (new Pair(text, prediction));
                });

        return (predictions);
    }
    
    private DocumentLabel predictOnce(Document aDocument)
    {   
        DocumentLabel   maxScoreCategory = null;
        Double          maxScore         = Double.NEGATIVE_INFINITY;

        for(Map.Entry<DocumentLabel, Double> categoryEntry : this._logPriors.entrySet())
        {
            DocumentLabel   category    = categoryEntry.getKey();
            Double          logProb     = categoryEntry.getValue();
            
            for(Map.Entry<String, Long> featureEntry : aDocument.getTokens().entrySet())
            {
                String feature = featureEntry.getKey();
                
                if(this._logConditionals.containsKey(feature)) //if the feature does not exist in the knowledge base skip it 
                {
                    Long occurrences = featureEntry.getValue(); //get its occurrences in text

                    logProb += occurrences * this._logConditionals.get(feature).get(category); //multiply loglikelihood score with occurrences
                }
            }
            
            if(logProb > maxScore)
            {
                maxScore            = logProb;
                maxScoreCategory    = category;
            }
        }
        return (maxScoreCategory); //return the category with highest score        
    }

    @Override
    public ClassificationReport test(Stream<RawText> aRawTexts)
    {
        ClassificationReport report = new ClassificationReport();
        
        this.predict(aRawTexts).peek((pair) -> 
        {    
            DocumentLabel label         = pair.getKey().getLabel();
            DocumentLabel prediction    = pair.getValue();
            
            ClassificationCount belonging = report.get(label);
            
            if (belonging == null)
                report.put(label, belonging = new ClassificationCount());
            
            ClassificationCount classified = report.get(prediction);
            
            if (classified == null)
                report.put(label, classified = new ClassificationCount());
            
            belonging.incrementBelonging();
            classified.incrementClassified();
            
            if (label.equals(prediction))
                classified.incrementCorrectlyClassified();
        }).count();
        
        return (report);
    }
}