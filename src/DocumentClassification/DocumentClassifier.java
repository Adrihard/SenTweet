package DocumentClassification;

import TextImportation.RawText;
import java.util.stream.Stream;
import javafx.util.Pair;

public interface DocumentClassifier
{
    public void                                     train(Stream<RawText> aRawTexts);
    public Stream<Pair<RawText, DocumentLabel>>     predict(Stream<RawText> aRawTexts);
    public ClassificationReport                     test(Stream<RawText> aRawTexts);
}