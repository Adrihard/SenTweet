package DocumentClassification;

/**
 *
 * @author Adrihard
 */
public abstract class ClassificationJob implements Runnable
{
    @Override
    public abstract void run();   
}
