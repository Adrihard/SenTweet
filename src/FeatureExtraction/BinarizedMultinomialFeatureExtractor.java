package FeatureExtraction;

import java.util.Map;
import java.util.Map.Entry;
import TextTokenization.Document;

public class BinarizedMultinomialFeatureExtractor extends MultinomialFeatureExtractor
{
    @Override
    protected Map<String, Long> extractFeaturesFrom(Document aDocument)
    {
        Map<String, Long> results = super.extractFeaturesFrom(aDocument);
        
        for (Entry<String, Long> result : results.entrySet())
           result.setValue((long) (result.getValue() == 0 ? 0 : 1));
        
        return (results);
    }
}