package FeatureExtraction;

import TextTokenization.Document;

public interface FeatureExtractor {
    public FeatureStats extractFeatures(Document aDocument);
}