package FeatureExtraction;

import java.util.Map;
import java.util.HashMap;
import DocumentClassification.DocumentLabel;

public class FeatureStats
{   
    private         long                                    _n;
    private final   Map<String, Map<DocumentLabel, Long>>   _featureCategoryJointCount;
    private final   Map<DocumentLabel, Long>                _categoryCount;
    
    public long getN() {
        return (this._n);
    }

    public void setN(long aSmallN) {
        this._n = aSmallN;
    }
    
    public Map<String, Map<DocumentLabel, Long>> getFeatureCategoryJointCount() {
        return (this._featureCategoryJointCount);
    }

    public java.util.Map<DocumentLabel, Long> getCategoryCount() {
        return (this._categoryCount);
    }
    
    public FeatureStats merge(FeatureStats aStats)
    {
        Map<String, Map<DocumentLabel, Long>> thisFcjc;
        Map<String, Map<DocumentLabel, Long>> aFcjc;
        
        thisFcjc    = this.getFeatureCategoryJointCount();
        aFcjc       = aStats.getFeatureCategoryJointCount();

        
        for (Map.Entry<String, Map<DocumentLabel, Long>> featEntry : aFcjc.entrySet())
        {
            String feature = featEntry.getKey();
            
            for (Map.Entry<DocumentLabel, Long> catEntry : featEntry.getValue().entrySet())
            {
                DocumentLabel               category    = catEntry.getKey();
                Long                        count       = catEntry.getValue();
                Map<DocumentLabel, Long>    joint       = thisFcjc.get(feature);
                
                if (joint == null)
                {    
                    thisFcjc.put(feature, new HashMap<>());
                    joint = thisFcjc.get(feature);
                }
                
                Long oldCount = joint.putIfAbsent(category, count);
                
                if (oldCount != null)
                    joint.put(category, oldCount + count);
            }
        }
        
        Map<DocumentLabel, Long> thisCatCount  = this.getCategoryCount();
        Map<DocumentLabel, Long> aCatCount     = aStats.getCategoryCount();
        
        for (Map.Entry<DocumentLabel, Long> catEntry : aCatCount.entrySet())
        {
            DocumentLabel   category    = catEntry.getKey();
            Long            count       = catEntry.getValue();
            
            Long oldCount = thisCatCount.putIfAbsent(category, count);

            if (oldCount != null)
                thisCatCount.put(category, oldCount + count); 
        }
        this.setN(this.getN() + aStats.getN());
        return this;
    }

    FeatureStats()
    {
        this._n = 0;
        this._featureCategoryJointCount = new HashMap<>();
        this._categoryCount = new HashMap<>();
    }
    
    public static FeatureStats empty() {
        return (new FeatureStats());
    }
    
    public FeatureStats(FeatureStats aStats)
    {
        this._n = aStats.getN();
        
        this._featureCategoryJointCount 
            = new HashMap(aStats.getFeatureCategoryJointCount());
        
        this._categoryCount = aStats.getCategoryCount();
    }
}