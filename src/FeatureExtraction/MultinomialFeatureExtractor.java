package FeatureExtraction;

import DocumentClassification.DocumentLabel;
import java.util.Map;
import java.util.HashMap;
import TextTokenization.Document;

public class MultinomialFeatureExtractor implements FeatureExtractor
{
    protected Map<String, Long> extractFeaturesFrom(Document aDocument) {
        return aDocument.getTokens();
    }

    @Override
    public FeatureStats extractFeatures(Document aDocument) {
        FeatureStats stats = new FeatureStats();
        Map<DocumentLabel, Long> categoryCount = stats.getCategoryCount();
        Map<String, Map<DocumentLabel, Long>> fcjc;
        fcjc = stats.getFeatureCategoryJointCount();

        DocumentLabel label = aDocument.getLabel();

        for (Map.Entry<String, Long> featureEntry : this.extractFeaturesFrom(aDocument).entrySet())
        {   
            String  feature         = featureEntry.getKey();
            Long    featureCount    = featureEntry.getValue();

            Map<DocumentLabel, Long> jointCounts = fcjc.get(feature);

            if (jointCounts == null)
                fcjc.put(feature, jointCounts = new HashMap());

            Long oldFeatureCount;

            oldFeatureCount = jointCounts.putIfAbsent(label, featureCount);

            if (oldFeatureCount != null)
                jointCounts.put(label, oldFeatureCount + featureCount);
        }

        Long oldCategoryCount;

        oldCategoryCount = categoryCount.putIfAbsent(label, new Long(1));

        if (oldCategoryCount != null)
            categoryCount.put(label, oldCategoryCount + 1);

        stats.setN(stats.getN() + 1);
        
        return stats;
    }
}