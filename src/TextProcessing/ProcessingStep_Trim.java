package TextProcessing;

/**
 *
 * @author Adrihard
 */
public class ProcessingStep_Trim extends ProcessingStep
{
    @Override
    public void execute(ProcessedText aText) {
        aText.setText(aText.getText().trim());
    }
}
