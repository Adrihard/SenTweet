package TextProcessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Adrihard
 */
public class ProcessingStep_RemoveFrequentWords extends ProcessingStep {

    private static final String FREQUENT_WORDS[] =
    {
        "a",
        "about",
        "after",
        "all",
        "also",
        "an",
        "and",
        "any",
        "as",
        "at",
        "back",
        "be",
        "because",
        "but",
        "by",
        "can",
        "come",
        "could",
        "day",
        "do",
        "even",
        "first",
        "for",
        "from",
        "get",
        "give",
        "go",
        "have",
        "he",
        "her",
        "him",
        "his",
        "how",
        "I",
        "if",
        "in",
        "into",
        "it",
        "its",
        "just",
        "know",
        "like",
        "look",
        "make",
        "me",
        "most",
        "my",
        "new",
        "no",
        "not",
        "now",
        "of",
        "on",
        "one",
        "only",
        "or",
        "other",
        "our",
        "out",
        "over",
        "people",
        "say",
        "see",
        "she",
        "so",
        "some",
        "take",
        "than",
        "that",
        "the",
        "their",
        "them",
        "then",
        "there",
        "these",
        "they",
        "think",
        "this",
        "time",
        "to",
        "two",
        "up",
        "us",
        "use",
        "want",
        "way",
        "we",
        "well",
        "what",
        "when",
        "which",
        "who",
        "will",
        "with",
        "work",
        "would",
        "year",
        "you",
        "your"
    };
    
    private final List<Pattern> _patterns;
    
    @Override
    public void execute(ProcessedText aText)
    {   
        String text = aText.getText();
        
        for (Pattern p : this._patterns)
            text = p.matcher(text).replaceAll("");
        
        aText.setText(text);
    }
    
    public ProcessingStep_RemoveFrequentWords()
    {
        this._patterns = new ArrayList<>();
        
        for (String word : ProcessingStep_RemoveFrequentWords.FREQUENT_WORDS)
            this._patterns.add(Pattern.compile("\\b" + word + "\\b"));
    }
    
}
