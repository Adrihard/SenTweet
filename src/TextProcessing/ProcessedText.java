package TextProcessing;

import DocumentClassification.DocumentLabel;
import TextImportation.RawText;

public class ProcessedText
{
    private String          _text;
    private DocumentLabel   _label;

    public String getText() {
        return (this._text);
    }

    public DocumentLabel getLabel() {
        return (this._label);
    }

    void setText(String aText) {
        this._text = aText;
    }
    
    void setLabel(DocumentLabel aLabel) {
        this._label = aLabel;
    }
    
    ProcessedText(String aText, DocumentLabel aLabel)
    {
        this._text = aText;
        this._label = aLabel;
    }
    
    ProcessedText(RawText aRawText)
    {
        this._text = aRawText.getText();
        this._label = aRawText.getLabel();
    }
}