package TextProcessing;

import java.util.ArrayList;
import java.util.List;
import TextImportation.RawText;

public class TextProcessor
{
    private final List<ProcessingStep> _processingSteps;

    public TextProcessor() {
        this._processingSteps = new ArrayList<>();
    }
    
    public TextProcessor(List<ProcessingStep> aProcessingSteps) {
        this._processingSteps = new ArrayList<>(aProcessingSteps);
    }

    public ProcessedText processText(RawText rawText)
    {
        ProcessedText text = new ProcessedText(rawText);
        _processingSteps.forEach(ps -> ps.execute(text));
        return text;    
    }

    public void addProcessingStep(ProcessingStep aProcessingStep) {
        this._processingSteps.add(aProcessingStep);
    }
    
    public void addProcessingSteps(List<ProcessingStep> aProcessingSteps) {
        this._processingSteps.addAll(aProcessingSteps);
    }
}