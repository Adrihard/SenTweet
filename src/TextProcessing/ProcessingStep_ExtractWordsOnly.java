package TextProcessing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Adrihard
 */
public class ProcessingStep_ExtractWordsOnly extends ProcessingStep 
{
    private static final    String  REGEX = "\\b\\w+\\b";
    
    private final           Pattern _pattern;
    
    @Override
    public void execute(ProcessedText aText)
    {   
        String text = aText.getText();
        
        Matcher m = this._pattern.matcher(text);
        
        StringBuilder str = new StringBuilder();
        
        if (m.find())
            str.append(m.group());
        
        while (m.find())
            str.append(" ").append(m.group());
        
        aText.setText(str.toString());
    }

    public ProcessingStep_ExtractWordsOnly() {
        this._pattern = Pattern.compile(REGEX);
    }
}
