package TextProcessing;

/**
 *
 * @author Adrihard
 */
public class ProcessingStep_LowercaseEverything extends ProcessingStep
{
    @Override
    public void execute (ProcessedText aText) {
        aText.setText(aText.getText().toLowerCase());
    }
}
