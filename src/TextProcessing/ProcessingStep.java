package TextProcessing;

public abstract class ProcessingStep {
    public abstract void execute(ProcessedText aText);
}