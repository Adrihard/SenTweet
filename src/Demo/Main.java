package Demo;

import DocumentClassification.ClassificationJob_LanguageDetection;
import DocumentClassification.ClassificationJob_Sentiment140;

/**
 *
 * @author Adrihard
 */
public class Main
{
    public static void main(String[] args)
    {   
        new Thread(new ClassificationJob_Sentiment140()).start();
    }
}
