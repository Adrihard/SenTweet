package TextTokenization;

import TextProcessing.ProcessedText;

public interface TextTokenizer {
    public Document tokenizeText(ProcessedText aText);
}