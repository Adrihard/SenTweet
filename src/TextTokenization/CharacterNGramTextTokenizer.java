package TextTokenization;

/**
 *
 * @author Adrihard
 */
public class CharacterNGramTextTokenizer extends NGramTextTokenizer
{
    private static final String SEPARATION_REGEX    = "";
    private static final String JOIN_STRING         = "";
    
    public CharacterNGramTextTokenizer(int aNMin, int aNMax){
        super(aNMin, aNMax);
    }

    @Override
    protected String getSeparationRegex() {
        return (CharacterNGramTextTokenizer.SEPARATION_REGEX);
    }

    @Override
    protected String getJoinString() {
        return (CharacterNGramTextTokenizer.JOIN_STRING);
    }
}