package TextTokenization;

import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import TextProcessing.ProcessedText;

public abstract class NGramTextTokenizer implements TextTokenizer
{
    private int _nMin;
    private int _nMax;
    
    protected abstract String getSeparationRegex();
    protected abstract String getJoinString(); 
    
    public int getNMin() {
        return this._nMin;
    }

    public int getNMax() {
        return this._nMax;
    }
    
    public void setNMin(int aNMin) {
        this._nMin = aNMin;
    }

    public void setNMax(int aNMax) {
        this._nMax = aNMax;
    }

    public NGramTextTokenizer(int aNMin, int aNMax)
    {
        this._nMin = aNMin;
        this._nMax = aNMax;
    }

    @Override
    public Document tokenizeText(ProcessedText aText)
    {
        String          text;
        List<String>    words;

        text    = aText.getText();
        words   = Arrays.asList(text.split(this.getSeparationRegex()));

        Map<String, Long>    tokens  = new HashMap<>();
        String jStr = this.getJoinString();

        int maxOffset = words.size() - this.getNMax() + 1;
        int stop = this.getNMax() - 1;
        for (int start = this.getNMin(); start <= maxOffset; ++start)
        {
            ++stop;

            for (int i = start; i <= stop; i++)
            {
                String token = String.join(jStr,
                        words.subList(start - this.getNMin(), i));

                Long oldValue = tokens.putIfAbsent(token, new Long(1));

                if (oldValue != null)
                    tokens.replace(token, oldValue + 1);
            }
        }

        return new Document(tokens, aText.getLabel());
    }
}