package TextTokenization;

/**
 *
 * @author Adrihard
 */
public class WordNGramTextTokenizer extends NGramTextTokenizer
{
    private static final String SEPARATION_REGEX    = "\\s+";
    private static final String JOIN_STRING         = " ";
    
    public WordNGramTextTokenizer(int aNMin, int aNMax){
        super(aNMin, aNMax);
    }

    @Override
    protected String getSeparationRegex() {
        return (WordNGramTextTokenizer.SEPARATION_REGEX);
    }

    @Override
    protected String getJoinString() {
        return (WordNGramTextTokenizer.JOIN_STRING);
    }
}