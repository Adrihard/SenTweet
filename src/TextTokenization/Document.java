package TextTokenization;

import java.util.Map;
import DocumentClassification.DocumentLabel;

public class Document 
{
    private final Map<String, Long>    _tokens;
    private final DocumentLabel        _label;

    public Map<String, Long> getTokens() {
        return this._tokens;
    }

    public DocumentLabel getLabel() {
        return this._label;
    }

    Document(Map<String, Long> aTokens, DocumentLabel aLabel)
    {
        this._tokens = aTokens;
        this._label = aLabel;
    }
}