package TextImportation;

import DocumentClassification.DocumentLabel;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface TextImporter {
    public Stream<RawText> importTexts(Path path);
    public Stream<RawText> importTexts(Path path, DocumentLabel label);
}