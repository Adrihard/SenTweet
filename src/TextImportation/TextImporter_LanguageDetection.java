package TextImportation;

import DocumentClassification.DocumentLabel;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class TextImporter_LanguageDetection implements TextImporter
{   
    @Override
    public Stream<RawText> importTexts(Path path) {
        return (Stream.empty());
    }

    private RawText buildRawText(String string, DocumentLabel label) {
        return (new RawText(string, label));
    }

    @Override
    public Stream<RawText> importTexts(Path path, DocumentLabel label)
    {
        try {
            return Files.lines(path)
                    .map(line -> buildRawText(line, label));
        } catch (IOException ex) {
            Logger.getLogger(TweetImporter_Sentiment140.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Stream.empty();
    }    
}