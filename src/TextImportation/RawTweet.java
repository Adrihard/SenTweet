package TextImportation;

import DocumentClassification.DocumentLabel;

/**
 *
 * @author Adrihard
 */
public class RawTweet extends RawText {
    
    private final String _username;
    
    public RawTweet
    (
        String          aText, 
        DocumentLabel   aLabel,
        String          aUsername
    )
    {
        super(aText, aLabel);
        this._username = aUsername;
    }
    
    public String getUsername() {
        return (this._username);
    }
    
}
