package TextImportation;

import DocumentClassification.DocumentLabel;

public class RawText
{
    private String          _text;
    private DocumentLabel   _label;

    public String getText() {
        return this._text;
    }

    public DocumentLabel getLabel() {
        return this._label;
    }

    public RawText(String aText, DocumentLabel aLabel)
    {
        this._text  = aText;
        this._label = aLabel;
    }
}