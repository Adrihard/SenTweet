package TextImportation;

import DocumentClassification.DocumentLabel;
import com.opencsv.CSVParser;
import java.io.IOException;
import com.opencsv.ICSVParser;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author Adrihard
 */
public class TweetImporter_Sentiment140 implements TweetImporter
{
    private final ICSVParser parser = new CSVParser(',', '"', '\0');
    
    @Override
    public Stream<RawText> importTexts(Path path) {
        try {
            return Files.lines(path, Charset.forName("Cp1252"))
                    .map(line -> readCsv(line))
                    .map(csv -> buildRawTweet(csv));
        } catch (IOException ex) {
            Logger.getLogger(TweetImporter_Sentiment140.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Stream.empty();
    }

    private RawTweet buildRawTweet(String[] csv) {
        DocumentLabel label;
        switch (csv[0])
        {
            case "0": label = DocumentLabel.Negative;
            break;
            case "2": label = DocumentLabel.Neutral;
            break;
            case "4": label = DocumentLabel.Positive;
            break;
            default: throw (new IllegalArgumentException("Unexpected Value \"" + csv[0] + "\" for tweet polarity."));
        }
        return new RawTweet(csv[5], label, csv[4]);
    }
    
    private String[] readCsv(String line)
    {
        try {
            return parser.parseLine(line);
        } catch (IOException ex) {
            Logger.getLogger(TweetImporter_Sentiment140.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new String[5];
    }
    
    @Override
    public Stream<RawTweet> importTweets(Path path)
    {
        try {
            return Files.lines(path, Charset.forName("Cp1252"))
                    .map(line -> readCsv(line))
                    .map(csv -> buildRawTweet(csv));
        } catch (IOException ex) {
            Logger.getLogger(TweetImporter_Sentiment140.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Stream.empty();
    }

    @Override
    public Stream<RawText> importTexts(Path path, DocumentLabel label) {
        return importTexts(path);
    }
}
