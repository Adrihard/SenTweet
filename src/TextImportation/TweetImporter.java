package TextImportation;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
 *
 * @author Adrihard
 */
public interface TweetImporter extends TextImporter {
    public Stream<RawTweet> importTweets(Path path);    
}
